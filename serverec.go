package serverec

import (
	"io"
	"net/http"
	"strings"
)

type ServeRec struct {
	W        http.ResponseWriter
	R        *http.Request
	Body     []byte
	Err      error
	Lnk      string
	IsMobile bool
	Data     map[string]interface{}

	f  string
	fa []string
}

func GetServeRec(w http.ResponseWriter, r *http.Request) (sr *ServeRec, err error) {
	rpa := strings.Split(strings.TrimLeft(r.URL.Path, "/"), "/")
	if len(rpa) == 0 {
		rpa = append(rpa, "")
	}
	sr = &ServeRec{
		W:        w,
		R:        r,
		f:        rpa[0],
		fa:       rpa[1:],
		Lnk:      strings.Join(rpa, "/"),
		IsMobile: isMobile(r),
		Data:     make(map[string]interface{}),
	}
	sr.Body, err = io.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	return sr, nil
}

func (sr *ServeRec) NextRoute() string {
	r := sr.f
	if len(sr.fa) == 0 {
		sr.fa = append(sr.fa, "")
	}
	sr.f = sr.fa[0]
	sr.fa = sr.fa[1:]
	return r
}

var mobileHeaders = []string{"Mobile"}

func isMobile(r *http.Request) bool {
	s := r.Header.Get("User-Agent")
	for _, d := range mobileHeaders {
		if strings.Contains(s, d) {
			return true
		}
	}
	return false
}
