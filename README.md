# ServeRec
Helper for serve HTTP request

## type ServeRec struct{}
- W `http.ResponseWriter`
- R `*http.Request`
- Body `[]byte`
- Err `error`
- Lnk `string`
- IsMobile `bool`
- Data `map[string]interface}|`
- NextRoute() `string`

## ServeRec.NextRoute()
```go
func (*ServeRec) NextRoute() string {}
```
